
# Simple Movie

### Prerequisite 

 - Flutter Version: 3.1.5 (recommendation)
 - Dart Version: 3.13.9 (recommendation)

 ### Demo
You can see the live demo app in:
 https://simple-movie-app-c2075.web.app

### Codebase
- Clean Architecture:
Clean Architecture really makes the app more maintanable by seperating the layers, as we see in the codebase the goals is to make the relationship between layers loose coupled/decoupled. So, for example if we want to change the `datasource` method we can just change the module, as long as the return object is still the same. Because of that, we don't need to modify the entire codebase for the change. Here is the visualization:
	![Clean Coder Blog](https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg)

In our code base the folder is seperated like this:

![enter image description here](https://gcdnb.pbrd.co/images/f9P307oBnynO.png?o=1)

The project goals is to separate the folder by layer as the Clean Architecture principle **(Separation of Concern)**. the project utility is in **core** folder such like dependency injections, module, and etc. (the more complex the app the more core utility we need).

The features files contains the feature/requirements of the project, separating feature by folders, so every feature folder has data, domain, and presentation layer.

As the principle of **Clean Architecture** we often use one of the principle of SOLID which is **Dependency Inversion**, we can see communication between `Repository` and `DataSource` there is an abstract class as the bridge or media for the communication/DTO so whenever we, for example, want to change the `DataSource` to get from local or anywhere else, we just need to change the logic in `DataSource` as long as the expected output still remains as before. In our app you can choose whether using in-memory(volatile) data source or persistence data (Hive).

![enter image description here](https://gcdnb.pbrd.co/images/70r2mZODO4dv.png?o=1)

Using this this approach really helps the developers to have a clear understanding about the objects, how the object interact to another and how the object have a single responsibility of its own function and scope.

### State Management
I am using MobX and GetX for the state management. Because, it is very simple, makes our development time faster, and powerful especially for our project scope. We don't have to make a boilerplate code for a small app.

### Data Source
Using In-memory (volatile) and Persistence data using Hive. you can just easily change the data source call in the repository if you want to swap the data source.

### Packages
|Packages|  Descriptions|
|--|--|
| google_fonts| Easy implement of material fonts, because we use monsterrat font|
| firebase_core| because we hosting our app, we need this for firebase configurations|
| MobX & GetX| We use MobX & GetX for the state management as it provides simple implementation but still can tackle many complex problems|
| dartz| Cutting boilerplate codes in DTO from data to presentation layer|
|mocktail| helps us to mock the class and also its parameter as well|
|hive_flutter|this package really helps us to store our state of the app like authentication and other specific tokens|
|auto_route| Really helps for page navigation, makes the code really simple and easy to understand. Also, very powerful in web navigation |
|uuid| for random generation id |
