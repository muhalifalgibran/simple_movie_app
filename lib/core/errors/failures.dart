abstract class Failure {}

class ClientError extends Failure {
  final String message;

  ClientError({
    this.message = 'Seems you have some problems..',
  });
}
