// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i3;
import 'package:flutter/material.dart' as _i6;
import 'package:simple_movie_app/features/movie/domain/entities/movie.dart'
    as _i4;
import 'package:simple_movie_app/features/movie/presentation/mobx/list_movie_mobx.dart'
    as _i5;
import 'package:simple_movie_app/features/movie/presentation/pages/crud_movie_page.dart'
    as _i1;
import 'package:simple_movie_app/features/movie/presentation/pages/list_movies_page.dart'
    as _i2;

abstract class $AppRouter extends _i3.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    CrudMovieRoute.name: (routeData) {
      final args = routeData.argsAs<CrudMovieRouteArgs>();
      return _i3.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i1.CrudMoviePage(
          movie: args.movie,
          mobX: args.mobX,
          key: args.key,
        ),
      );
    },
    ListMoviesRoute.name: (routeData) {
      return _i3.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.ListMoviesPage(),
      );
    },
  };
}

/// generated route for
/// [_i1.CrudMoviePage]
class CrudMovieRoute extends _i3.PageRouteInfo<CrudMovieRouteArgs> {
  CrudMovieRoute({
    _i4.Movie? movie,
    required _i5.ListMovieMobX mobX,
    _i6.Key? key,
    List<_i3.PageRouteInfo>? children,
  }) : super(
          CrudMovieRoute.name,
          args: CrudMovieRouteArgs(
            movie: movie,
            mobX: mobX,
            key: key,
          ),
          initialChildren: children,
        );

  static const String name = 'CrudMovieRoute';

  static const _i3.PageInfo<CrudMovieRouteArgs> page =
      _i3.PageInfo<CrudMovieRouteArgs>(name);
}

class CrudMovieRouteArgs {
  const CrudMovieRouteArgs({
    this.movie,
    required this.mobX,
    this.key,
  });

  final _i4.Movie? movie;

  final _i5.ListMovieMobX mobX;

  final _i6.Key? key;

  @override
  String toString() {
    return 'CrudMovieRouteArgs{movie: $movie, mobX: $mobX, key: $key}';
  }
}

/// generated route for
/// [_i2.ListMoviesPage]
class ListMoviesRoute extends _i3.PageRouteInfo<void> {
  const ListMoviesRoute({List<_i3.PageRouteInfo>? children})
      : super(
          ListMoviesRoute.name,
          initialChildren: children,
        );

  static const String name = 'ListMoviesRoute';

  static const _i3.PageInfo<void> page = _i3.PageInfo<void>(name);
}
