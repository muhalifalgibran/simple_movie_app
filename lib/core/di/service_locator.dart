import 'package:get/get.dart';
import 'package:simple_movie_app/features/movie/data/datasources/movie_data_source.dart';
import 'package:simple_movie_app/features/movie/data/datasources/movie_hive_data_source.dart';
import 'package:simple_movie_app/features/movie/data/repositories/movie_repository_impl.dart';
import 'package:simple_movie_app/features/movie/domain/repositories/movie_repository.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/create_movie.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/delete_movie.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/get_movies.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/update_movie.dart';

void configureLocator() {
  // datasources
  Get.put<MovieDataSource>(MovieDataSourceImpl());
  Get.put<MovieHiveDataSource>(MovieHiveDataSourceImpl());

  // repositories
  Get.put<MovieRepository>(MovieRepositoryImpl());

  // usecases
  Get.put<GetMovies>(GetMovies());
  Get.put<CreateMovie>(CreateMovie());
  Get.put<DeleteMovie>(DeleteMovie());
  Get.put<UpdateMovie>(UpdateMovie());
}
