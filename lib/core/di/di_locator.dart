import 'package:get_it/get_it.dart';
import 'package:simple_movie_app/features/movie/data/datasources/movie_data_source.dart';
import 'package:simple_movie_app/features/movie/data/datasources/movie_hive_data_source.dart';
import 'package:simple_movie_app/features/movie/data/repositories/movie_repository_impl.dart';
import 'package:simple_movie_app/features/movie/domain/repositories/movie_repository.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/create_movie.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/delete_movie.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/get_movies.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/update_movie.dart';

final getIt = GetIt.instance;

void setup() {
  getIt.registerLazySingleton<MovieDataSource>(() => MovieDataSourceImpl());
  getIt.registerLazySingleton<MovieHiveDataSource>(
    () => MovieHiveDataSourceImpl(),
  );

  getIt.registerLazySingleton<MovieRepository>(() => MovieRepositoryImpl());

  getIt.registerLazySingleton<GetMovies>(() => GetMovies());
  getIt.registerLazySingleton<CreateMovie>(() => CreateMovie());
  getIt.registerLazySingleton<DeleteMovie>(() => DeleteMovie());
  getIt.registerLazySingleton<UpdateMovie>(() => UpdateMovie());
}
