import 'package:uuid/uuid.dart';

class Movie {
  final String id, title, director, summary;
  final List<String> genres;

  const Movie({
    required this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.genres,
  });

  static List<Movie> generate() {
    return [
      Movie(
        id: const Uuid().v1(),
        title: 'Spongebob',
        director: 'Stephen Hillenburg',
        summary: 'Energetic sea sponge navigates quirky underwater adventures.',
        genres: const [
          'Drama',
          'Action',
          'Animation',
          'Sci-fi',
          'Horror',
        ],
      ),
      Movie(
        id: const Uuid().v1(),
        title: 'Red Notice',
        director: 'Rawson Marshall Thurber',
        summary:
            '"Red Notice": A heist, art, and comedy mashup directed by Rawson Marshall Thurber.',
        genres: const [
          'Action',
          'Sci-fi',
        ],
      ),
      Movie(
        id: const Uuid().v1(),
        title: 'Cigarette Girl',
        director: 'Nayato Fio Nuala',
        summary:
            'A drama highlighting a young woman\'s journey in the clove cigarette industry',
        genres: const [
          'Drama',
          'Action',
        ],
      ),
      Movie(
        id: const Uuid().v1(),
        title: 'Spongebob 2',
        director: 'Stephen Hillenburg',
        summary: 'Spongebob an Patrick having a good time.',
        genres: const [
          'Animation',
          'Sci-fi',
          'Horror',
        ],
      ),
    ];
  }
}
