import 'package:dartz/dartz.dart';
import 'package:simple_movie_app/core/errors/failures.dart';
import 'package:simple_movie_app/features/movie/domain/entities/movie.dart';

abstract class MovieRepository {
  Future<Either<Failure, List<Movie>>> getMovies();
  Future<Either<Failure, void>> deleteMovie(String id);
  Future<Either<Failure, void>> createMovie(Movie movie);
  Future<Either<Failure, void>> updateMovie(Movie movie);
}
