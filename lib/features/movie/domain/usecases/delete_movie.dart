import 'package:dartz/dartz.dart';
import 'package:get/get.dart';
import 'package:simple_movie_app/core/di/di_locator.dart';
import 'package:simple_movie_app/core/errors/failures.dart';
import 'package:simple_movie_app/features/movie/domain/repositories/movie_repository.dart';

class DeleteMovie {
  final MovieRepository _repo = getIt<MovieRepository>();

  Future<Either<Failure, void>> call(String id) async =>
      await _repo.deleteMovie(id);
}
