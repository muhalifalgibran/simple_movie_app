import 'package:simple_movie_app/features/movie/domain/entities/movie.dart';

class MovieModel extends Movie {
  const MovieModel({
    required super.id,
    required super.title,
    required super.director,
    required super.summary,
    required super.genres,
  });

  factory MovieModel.fromJson(Map<dynamic, dynamic> json) => MovieModel(
        id: json['id'],
        title: json['title'],
        director: json['director'],
        summary: json['summary'],
        genres: json['genres'],
      );

  static Map<dynamic, dynamic> toJson(Movie movie) => {
        'id': movie.id,
        'title': movie.title,
        'director': movie.director,
        'summary': movie.summary,
        'genres': movie.genres,
      };

  static Map<dynamic, dynamic> toJsonList(List<Movie> movies) {
    return {
      'data': movies.map((e) => toJson(e)).toList(),
    };
  }
}
