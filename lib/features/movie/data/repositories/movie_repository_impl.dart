import 'package:dartz/dartz.dart';
import 'package:simple_movie_app/core/di/di_locator.dart';
import 'package:simple_movie_app/core/errors/failures.dart';
import 'package:simple_movie_app/features/movie/data/datasources/movie_data_source.dart';
import 'package:simple_movie_app/features/movie/data/datasources/movie_hive_data_source.dart';
import 'package:simple_movie_app/features/movie/domain/entities/movie.dart';
import 'package:simple_movie_app/features/movie/domain/repositories/movie_repository.dart';

class MovieRepositoryImpl implements MovieRepository {
  /*
  Keep in mind that we can use two datasource. just comment and uncomment
  the data source we want to use. but as our requirements needs we're just using
  in-memory(volatile) data by default not persistance data.
  */

  final MovieDataSource _dataSource = getIt<MovieDataSource>();
  // final _dataSource = getIt<MovieHiveDataSource>();
  // final MovieHiveDataSource _dataSource = getIt.get<MovieHiveDataSource>();
  @override
  Future<Either<Failure, void>> createMovie(Movie movie) async {
    try {
      _dataSource.createMovie(movie);
      return const Right(null);
    } catch (e) {
      return Left(ClientError(message: e.toString()));
    }
  }

  @override
  Future<Either<Failure, void>> updateMovie(Movie movie) async {
    try {
      _dataSource.updateMovie(movie);
      return const Right(null);
    } catch (e) {
      return Left(ClientError(message: e.toString()));
    }
  }

  @override
  Future<Either<Failure, void>> deleteMovie(String id) async {
    try {
      _dataSource.deleteMovie(id);
      return const Right(null);
    } catch (e) {
      return Left(ClientError(message: e.toString()));
    }
  }

  @override
  Future<Either<Failure, List<Movie>>> getMovies() async {
    try {
      final data = await _dataSource.getMovies();
      return Right(data);
    } catch (e) {
      return Left(ClientError(message: e.toString()));
    }
  }
}
