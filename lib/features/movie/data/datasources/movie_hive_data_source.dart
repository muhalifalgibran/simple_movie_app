import 'package:hive_flutter/hive_flutter.dart';
import 'package:simple_movie_app/features/movie/data/model/movie_model.dart';
import 'package:simple_movie_app/features/movie/domain/entities/movie.dart';

/* we should not catch the error here since we treat our repository
as DTO so our repository should return whether an error or the expected object
*/
abstract class MovieHiveDataSource {
  Future<List<Movie>> getMovies();
  Future<void> deleteMovie(String id);
  Future<void> updateMovie(Movie movie);
  Future<void> createMovie(Movie movie);
}

class MovieHiveDataSourceImpl implements MovieHiveDataSource {
  @override
  Future<void> createMovie(Movie movie) async {
    // open box
    final box = Hive.box('movie-collection');

    // get data
    Map<dynamic, dynamic> json = box.get('movies');
    List<Movie> data =
        json['data'].map<Movie>((json) => MovieModel.fromJson(json)).toList();

    // insert new data
    data.add(movie);

    // modeling again to map
    var newData = MovieModel.toJsonList(data);

    // put again in the box
    box.put('movies', newData);
  }

  @override
  Future<void> deleteMovie(String id) async {
    // open box
    final box = Hive.box('movie-collection');

    // get data
    Map<dynamic, dynamic> json = box.get('movies');
    List<Movie> data =
        json['data'].map<Movie>((json) => MovieModel.fromJson(json)).toList();

    // delete data
    data.removeWhere((element) => element.id == id);

    // modeling again to map
    var newData = MovieModel.toJsonList(data);

    // put again in the box
    box.put('movies', newData);
  }

  @override
  Future<List<Movie>> getMovies() async {
    // we store the collection to hive first if the box is empty
    // and formatting as map(primitive) because we don't specify the entity as
    // hive object
    final box = await Hive.openBox('movie-collection');
    if (box.get('movies') == null) {
      var movies = Movie.generate();
      var data = MovieModel.toJsonList(movies);
      box.put('movies', data);
    }
    Map<dynamic, dynamic> json = box.get('movies');

    var result =
        json['data'].map<Movie>((json) => MovieModel.fromJson(json)).toList();

    return result;
  }

  @override
  Future<void> updateMovie(Movie movie) async {
    // open box
    final box = Hive.box('movie-collection');

    // get data
    Map<dynamic, dynamic> json = box.get('movies');
    List<Movie> data =
        json['data'].map<Movie>((json) => MovieModel.fromJson(json)).toList();

    // modify data
    var index = data.indexWhere((element) => element.id == movie.id);
    data[index] = movie;

    // modeling again to map
    var newData = MovieModel.toJsonList(data);

    // put again in the box
    box.put('movies', newData);
  }
}
