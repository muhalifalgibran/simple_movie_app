import 'package:simple_movie_app/features/movie/domain/entities/movie.dart';

/* we should not catch the error here since we treat our repository
as DTO so our repository should return whether an error or the expected object
*/
abstract class MovieDataSource {
  List<Movie> getMovies();
  void deleteMovie(String id);
  void createMovie(Movie movie);
  void updateMovie(Movie movie);
}

class MovieDataSourceImpl implements MovieDataSource {
  List<Movie> movies = Movie.generate();

  @override
  void createMovie(Movie movie) {
    movies.add(movie);
  }

  @override
  void updateMovie(Movie movie) {
    var index = movies.indexWhere((element) => element.id == movie.id);
    movies[index] = movie;
  }

  @override
  void deleteMovie(String id) {
    movies.removeWhere((element) => element.id == id);
  }

  @override
  List<Movie> getMovies() {
    return movies;
  }
}
