// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_movie_mobx.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ListMovieMobX on _ListMovieMobX, Store {
  late final _$movieAtom = Atom(name: '_ListMovieMobX.movie', context: context);

  @override
  List<Movie> get movie {
    _$movieAtom.reportRead();
    return super.movie;
  }

  @override
  set movie(List<Movie> value) {
    _$movieAtom.reportWrite(value, super.movie, () {
      super.movie = value;
    });
  }

  late final _$originalDataMovieAtom =
      Atom(name: '_ListMovieMobX.originalDataMovie', context: context);

  @override
  List<Movie> get originalDataMovie {
    _$originalDataMovieAtom.reportRead();
    return super.originalDataMovie;
  }

  @override
  set originalDataMovie(List<Movie> value) {
    _$originalDataMovieAtom.reportWrite(value, super.originalDataMovie, () {
      super.originalDataMovie = value;
    });
  }

  late final _$errorAtom = Atom(name: '_ListMovieMobX.error', context: context);

  @override
  Failure? get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(Failure? value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  late final _$getMovieAsyncAction =
      AsyncAction('_ListMovieMobX.getMovie', context: context);

  @override
  Future<dynamic> getMovie() {
    return _$getMovieAsyncAction.run(() => super.getMovie());
  }

  late final _$_ListMovieMobXActionController =
      ActionController(name: '_ListMovieMobX', context: context);

  @override
  void searchMovie(String title) {
    final _$actionInfo = _$_ListMovieMobXActionController.startAction(
        name: '_ListMovieMobX.searchMovie');
    try {
      return super.searchMovie(title);
    } finally {
      _$_ListMovieMobXActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
movie: ${movie},
originalDataMovie: ${originalDataMovie},
error: ${error}
    ''';
  }
}
