import 'package:get/get.dart';
import 'package:mobx/mobx.dart';
import 'package:simple_movie_app/core/di/di_locator.dart';
import 'package:simple_movie_app/core/errors/failures.dart';
import 'package:simple_movie_app/features/movie/domain/entities/movie.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/get_movies.dart';

part 'list_movie_mobx.g.dart';

class ListMovieMobX = _ListMovieMobX with _$ListMovieMobX;

abstract class _ListMovieMobX with Store {
  @observable
  List<Movie> movie = [];

  @observable
  List<Movie> originalDataMovie = [];

  @observable
  Failure? error;

  @action
  Future getMovie() async {
    final api = await getIt<GetMovies>().call(NoParams());

    api.fold(
      (failure) => error = failure,
      (data) {
        // reversed to sort the data by the newest
        movie = data.reversed.toList();
        originalDataMovie = data.reversed.toList();
      },
    );
  }

  @action
  void searchMovie(String title) {
    if (title == '') {
      movie = originalDataMovie;
      return;
    }
    movie = originalDataMovie
        .where((element) =>
            element.title.toLowerCase().contains(title.toLowerCase()))
        .toList();
  }
}
