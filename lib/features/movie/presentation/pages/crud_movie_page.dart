import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:simple_movie_app/features/movie/domain/entities/movie.dart';
import 'package:simple_movie_app/features/movie/presentation/getx/crud_movie_getx.dart';
import 'package:simple_movie_app/features/movie/presentation/mobx/list_movie_mobx.dart';
import 'package:simple_movie_app/features/movie/presentation/widgets/custom_chip.dart';
import 'package:simple_movie_app/features/movie/presentation/widgets/custom_input_text.dart';
import 'package:simple_movie_app/features/movie/presentation/widgets/neu_container.dart';
import 'package:uuid/uuid.dart';

@RoutePage()
class CrudMoviePage extends StatefulWidget {
  final Movie? movie;
  final ListMovieMobX mobX;
  const CrudMoviePage({
    this.movie,
    required this.mobX,
    Key? key,
  }) : super(key: key);

  @override
  CrudMoviePageState createState() => CrudMoviePageState();
}

class CrudMoviePageState extends State<CrudMoviePage> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _titleCtrl;
  late TextEditingController _dirCtrl;
  late TextEditingController _summCtrl;
  final List<String> _genres = [];
  static const List<String> _labels = [
    'Horror',
    'Sci-fi',
    'Action',
    'Drama',
    'Animation',
  ];

  void setGenres(bool isRemove, String label) {
    if (isRemove) {
      _genres.remove(label);
    } else {
      _genres.add(label);
    }
    print(_genres);
  }

  @override
  void initState() {
    super.initState();
    if (widget.movie != null) {
      _genres.addAll(widget.movie!.genres);
    }
    _titleCtrl = TextEditingController(text: widget.movie?.title);
    _dirCtrl = TextEditingController(text: widget.movie?.director);
    _summCtrl = TextEditingController(text: widget.movie?.summary);
  }

  final CrudMovieGetX _getXController = CrudMovieGetX();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        leading: GestureDetector(
          onTap: AutoRouter.of(context).pop,
          child: const Icon(
            Icons.chevron_left,
            color: Colors.white,
          ),
        ),
        actions: [
          Visibility(
            visible: widget.movie != null,
            child: GestureDetector(
              onTap: () {
                confirmDelete(widget.movie!.id);
              },
              child: const Icon(
                Icons.delete,
                color: Colors.redAccent,
              ),
            ),
          ),
          const SizedBox(width: 20),
          GestureDetector(
            onTap: () {
              if (_formKey.currentState!.validate() && _genres.isNotEmpty) {
                Movie movie = Movie(
                  id: widget.movie?.id ?? const Uuid().v1(),
                  title: _titleCtrl.text,
                  director: _dirCtrl.text,
                  summary: _summCtrl.text,
                  genres: _genres,
                );
                confirmSave(movie);
              }
            },
            child: const Icon(
              Icons.save,
              color: Colors.white,
            ),
          ),
          const SizedBox(width: 24),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                // title
                NeuContainer(
                  child: CustomInputText(
                    controller: _titleCtrl,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    label: 'Title',
                    hint: 'input title..',
                    // initialValue: widget.movie?.title,
                  ),
                ),
                const SizedBox(height: 20),
                // director
                NeuContainer(
                  child: CustomInputText(
                    controller: _dirCtrl,
                    label: 'Director',
                    hint: 'input director..',
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    // initialValue: widget.movie?.director,
                  ),
                ),
                const SizedBox(height: 20),
                // summary
                NeuContainer(
                  child: CustomInputText(
                    controller: _summCtrl,
                    label: 'Summary',
                    hint: 'input summary..',
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    // initialValue: widget.movie?.summary,
                    maxLines: 5,
                    maxCharacter: 100,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                // chips
                Wrap(
                  runSpacing: 12,
                  spacing: 12,
                  children: List.generate(
                    _labels.length,
                    (index) => CustomChip(
                      label: _labels[index],
                      isActive: widget.movie?.genres.contains(_labels[index]) ??
                          false,
                      onActive: (value) {
                        setGenres(
                          value == null,
                          _labels[index],
                        );
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void confirmSave(Movie movie) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const SizedBox(height: 14),
                const Text(
                  'Confirm create/update?',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                const SizedBox(height: 14),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('Close'),
                    ),
                    TextButton(
                      onPressed: () {
                        // Execute whether it is update or create
                        widget.movie == null
                            ? _getXController.createMovie(movie)
                            : _getXController.updateMovie(movie);

                        widget.mobX.getMovie();

                        AutoRouter.of(context).popUntilRoot();
                      },
                      child: const Text('Confirm'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void confirmDelete(String id) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const SizedBox(height: 14),
                const Text(
                  'Confirm delete?',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                const SizedBox(height: 14),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('Close'),
                    ),
                    TextButton(
                      onPressed: () {
                        _getXController.deleteMovie(id);
                        widget.mobX.getMovie();

                        AutoRouter.of(context).popUntilRoot();
                      },
                      child: const Text('Confirm'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
