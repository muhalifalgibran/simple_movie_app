import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:simple_movie_app/core/routes/app_router.gr.dart';
import 'package:simple_movie_app/features/movie/presentation/mobx/list_movie_mobx.dart';
import 'package:simple_movie_app/features/movie/presentation/widgets/movie_list_widget.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

@RoutePage()
class ListMoviesPage extends StatefulWidget {
  const ListMoviesPage({Key? key}) : super(key: key);

  @override
  ListMoviesPageState createState() => ListMoviesPageState();
}

class ListMoviesPageState extends State<ListMoviesPage> {
  final ListMovieMobX _movieStore = ListMovieMobX();
  @override
  void initState() {
    super.initState();
    _movieStore.getMovie();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Text(
          'Movies Collection',
          style: TextStyle(
            fontSize: 24,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(60.0),
          child: searchWidget(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          AutoRouter.of(context).push(CrudMovieRoute(
            mobX: _movieStore,
          ));
        },
        backgroundColor: Colors.deepPurple,
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      body: Observer(builder: (_) {
        return SingleChildScrollView(
          child: Column(
            children: List.generate(
              _movieStore.movie.length,
              (index) => Container(
                margin: const EdgeInsets.all(20.0),
                child: GestureDetector(
                  onTap: () {
                    AutoRouter.of(context).push(
                      CrudMovieRoute(
                        movie: _movieStore.movie[index],
                        mobX: _movieStore,
                      ),
                    );
                  },
                  child: MovieListWidget(
                    movie: _movieStore.movie[index],
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }

  Widget searchWidget() {
    return Container(
      padding: const EdgeInsets.all(12),
      margin: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 12,
      ),
      decoration: BoxDecoration(
        color: Colors.grey.shade300,
        border: Border.all(),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          const Icon(Icons.search),
          const SizedBox(width: 10),
          Expanded(
            child: TextFormField(
              autofocus: false,
              decoration: const InputDecoration.collapsed(
                hintText: 'search',
              ),
              onChanged: (value) {
                _movieStore.searchMovie(value);
              },
            ),
          ),
        ],
      ),
    );
  }
}
