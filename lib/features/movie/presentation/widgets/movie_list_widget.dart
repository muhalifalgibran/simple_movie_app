import 'package:flutter/widgets.dart';
import 'package:simple_movie_app/features/movie/domain/entities/movie.dart';
import 'package:simple_movie_app/features/movie/presentation/widgets/neu_container.dart';

class MovieListWidget extends StatelessWidget {
  final Movie movie;
  const MovieListWidget({required this.movie, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NeuContainer(
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Wrap(
          direction: Axis.vertical,
          children: [
            Text(
              movie.title,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 12),
            Text(
              movie.director,
              style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.normal,
              ),
            ),
            const SizedBox(height: 4),
            SizedBox(
              width: MediaQuery.of(context).size.width - 80,
              child: Text(
                movie.summary,
                overflow: TextOverflow.ellipsis,
                maxLines: 3,
                style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
            const SizedBox(height: 60),
            SizedBox(
              width: 310,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    formatGenres(movie.genres),
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String formatGenres(List<String> genres) => genres.join(" / ");
}
