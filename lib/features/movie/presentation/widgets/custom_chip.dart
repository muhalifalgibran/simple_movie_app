import 'package:flutter/material.dart';
import 'package:simple_movie_app/features/movie/presentation/widgets/neu_container.dart';

class CustomChip extends StatefulWidget {
  final String label;
  final Function(String?) onActive;
  final bool isActive;
  const CustomChip({
    required this.label,
    required this.onActive,
    this.isActive = false,
    Key? key,
  }) : super(key: key);

  @override
  State<CustomChip> createState() => _CustomChipState();
}

class _CustomChipState extends State<CustomChip> {
  bool isActive = false;

  @override
  void initState() {
    super.initState();
    isActive = widget.isActive;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isActive = !isActive;
        });
        if (isActive) {
          widget.onActive(widget.label);
        } else {
          widget.onActive(null);
        }
      },
      child: NeuContainer(
        backgroundColor: isActive ? Colors.deepPurple : Colors.grey.shade300,
        child: Text(
          widget.label,
          style: TextStyle(
            color: isActive ? Colors.white : Colors.black,
          ),
        ),
      ),
    );
  }
}
