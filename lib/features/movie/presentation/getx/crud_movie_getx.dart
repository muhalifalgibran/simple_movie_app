import 'package:get/get.dart';
import 'package:simple_movie_app/core/di/di_locator.dart';
import 'package:simple_movie_app/features/movie/domain/entities/movie.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/create_movie.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/delete_movie.dart';
import 'package:simple_movie_app/features/movie/domain/usecases/update_movie.dart';

enum CrudEnumState {
  initial,
  loading,
  success,
  failed,
}

class CrudMovieGetX extends GetxController {
  final status = CrudEnumState.initial.obs;
  void createMovie(Movie movie) async {
    final api = await getIt<CreateMovie>().call(movie);
    // give it, because this call is very fast so it's better
    // to make delay for better user experience
    await Future.delayed(const Duration(milliseconds: 750));
    api.fold(
      (failure) => status.value = CrudEnumState.failed,
      (data) => status.value = CrudEnumState.success,
    );
  }

  void updateMovie(Movie movie) async {
    final api = await getIt<UpdateMovie>().call(movie);
    // give it, because this call is very fast so it's better
    // to make delay for better user experience
    await Future.delayed(const Duration(milliseconds: 750));
    api.fold(
      (failure) => status.value = CrudEnumState.failed,
      (data) => status.value = CrudEnumState.success,
    );
  }

  void deleteMovie(String id) async {
    final api = await getIt<DeleteMovie>().call(id);
    // give it, because this call is very fast so it's better
    // to make delay for better user experience
    await Future.delayed(const Duration(milliseconds: 750));
    api.fold(
      (failure) => status.value = CrudEnumState.failed,
      (data) => status.value = CrudEnumState.success,
    );
  }
}
