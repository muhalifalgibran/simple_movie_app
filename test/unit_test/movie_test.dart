import 'package:flutter_test/flutter_test.dart';
import 'package:simple_movie_app/features/movie/domain/entities/movie.dart';

void main() {
  test('should return list of Movie when generate call', () {
    expect(Movie.generate(), isA<List<Movie>>());
  });
}
