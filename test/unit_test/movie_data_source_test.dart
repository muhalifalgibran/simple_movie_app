import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:simple_movie_app/features/movie/data/datasources/movie_data_source.dart';
import 'package:simple_movie_app/features/movie/domain/entities/movie.dart';

class MockMovie extends Mock implements Movie {}

void main() {
  late MovieDataSourceImpl dataSource;
  late Movie movies;

  setUp(() {
    movies = MockMovie();
    registerFallbackValue(movies);
    dataSource = MovieDataSourceImpl();
  });

  test('should add new movie', () {
    dataSource.createMovie(
      const Movie(
        id: 'id3',
        title: 'title',
        director: 'director',
        summary: 'summary',
        genres: ['1', '2', '2'],
      ),
    );

    // the generate class should generate 4 items, so when we add new
    // it should be 5
    expect(dataSource.movies.length, 5);
  });

  test('should delete movie', () {
    String firstItemId = dataSource.movies.first.id;
    dataSource.deleteMovie(firstItemId);

    // the generate class should generate 4 items, so when we delete
    // it should be 3
    expect(dataSource.movies.length, 3);
  });

  test('should get movies', () {
    dataSource.getMovies();

    // the generate class should generate 4 items;
    expect(dataSource.movies.length, 4);
  });

  test('should update existing move', () {
    String firstItemId = dataSource.movies.first.id;

    dataSource.updateMovie(
      Movie(
        id: firstItemId,
        title: 'new title',
        director: 'director',
        summary: 'summary',
        genres: ['1', '2', '2'],
      ),
    );

    /* 
    the generate class should generate 4 items and when we update
    (the first item for example) it is should be remains 4.
    but, the first item should be an updated item
    */
    expect(dataSource.movies.length, 4);
    expect(dataSource.movies.first.title, 'new title');
  });
}
