import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:simple_movie_app/features/movie/data/datasources/movie_data_source.dart';
import 'package:simple_movie_app/features/movie/data/repositories/movie_repository_impl.dart';
import 'package:simple_movie_app/features/movie/domain/repositories/movie_repository.dart';

import '../utils.dart';

class MockMovieDataSource extends Mock implements MovieDataSource {}

void main() {
  late MovieDataSource dataSource;
  late MovieRepository repository;

  setUpAll(() {
    dataSource = MockMovieDataSource();
    registerTestLazySingleton<MovieDataSource>(dataSource);
    repository = MovieRepositoryImpl();
  });

  test('Should return left when exception thrown from getMovies', () async {
    // arrange
    when(() => dataSource.getMovies()).thenThrow(Exception('error!'));

    // act
    final api = await repository.getMovies();

    // assert
    expect(
      api,
      isA<Left>(),
    );
  });

  test('Should return Right success getMovies', () async {
    // arrange
    when(() => dataSource.getMovies()).thenAnswer((_) => []);

    // act
    final api = await repository.getMovies();

    // assert
    expect(
      api,
      isA<Right>(),
    );
  });
}
